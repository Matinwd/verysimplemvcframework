<?php


namespace App\Helpers;
class MySession
{
    private $name;
    private $message;
    private $class;

    public function __construct(string $name, string $message = null, string $class = 'alert alert-primary')
    {
        $this->name = $name;
        $this->message = $message;
        $this->class = $class;
    }

    public function setFlash(): void
    {
        if (!isset($this->name)) {
            throw new \Exception("Missing parameter {$this->name} for method MySession::flash");
        }
        if (isset($this->name) and isset($this->message)) {
            $_SESSION[$this->name] = $this->message;
            $_SESSION[$this->name . '_class'] = $this->class;
        }
        if (isset($_SESSION[$this->name]) and !isset($this->message)) {
            echo "<div class='{$_SESSION[$this->name . '_class']}' >{$_SESSION[$this->name]}</div>'";
            unset($_SESSION[$this->name]);
        }
    }
}