<?php


use App\Helpers\MyMailer;
use App\Helpers\MySession;

function myFlash($name, $message = null, $class = 'alert alert-primary')
{
    (new MySession($name, $message, $class))->setFlash();
}

function sendMail($email, $messages, $files)
{
    (new MyMailer())->email($email, $messages, $files);
}

function redirect($redirectDestination)
{
    header('location: ' . __APP_ROOT__ . $redirectDestination);
}