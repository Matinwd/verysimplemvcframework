<?php

class Database
{

    private $hostName;
    private $dbName;
    private $userName;
    private $pass;

    private $pdo;
    private static $instance;

    private $stmt;


    public function __construct()
    {
        $this->dbName = __DB_NAME__;
        $this->hostName = __DB_HOST__;
        $this->pass = __DB_PASS__;
        $this->userName = __DB_USER__;
        $this->pdo = new PDO("mysql:host{$this->hostName};dbname={$this->dbName}", "{$this->userName}", "{$this->pass}");
        $this->pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    }

    public function getInstance()
    {
        if (self::$instance == null) {
            self::$instance = new self;
        }
    }

    public function getConnection()
    {
        return $this->pdo;
    }

    public function query(string $querySql)
    {
        try {
            $this->stmt = $this->pdo->prepare($querySql);
        } catch (PDOException $e) {
            echo $e->getMessage();
        }
    }

    public function bind($param, $value)
    {
        $this->stmt->bindValue(':' . $param, $value);
    }

    public function execute()
    {
        return ($this->stmt->execute());


    }

    public function resultAll()
    {
        return ($this->stmt->fetchAll(PDO::FETCH_OBJ));
    }

    public function resultOne()
    {
        return $this->stmt->fetch(PDO::FETCH_OBJ);
    }

    public function count()
    {
        return $this->stmt->rowCount();
    }
}