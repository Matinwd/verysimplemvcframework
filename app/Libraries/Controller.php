<?php

class Controller
{
    public function model($modelName)
    {
        if (!file_exists('../app/Models/' . $modelName . '.php')) {
            die('model deos not exists');
        }

        require_once '../app/Models/' . $modelName . '.php';
        return new  $modelName;
    }

    public function view($viewName, $data = [])
    {
        if (!file_exists('../app/Views/' . $viewName . '.php')) {
            die('View Does not exists');
        }
        require_once '../app/Views/' . $viewName . '.php';

    }
}