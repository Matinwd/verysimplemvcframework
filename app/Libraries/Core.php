<?php

class Core
{

    private $currentController = 'Pages';

    private $currentMethod = 'index';

    private $dependencies = [];

    public function __construct()
    {
    }

    public function getURL()
    {
        $url = [];
        if (isset($_GET['url'])) {

            // explode string and set it to an array
            $url = explode('/', rtrim($_GET['url']));
        }
        // get Controller from address and set it
        $url = $this->getController($url);

        //get Method from address and set it
        $url = $this->getMethod($url);

        // get dependencies from address and set it
        $this->getDependencies($url);

        // call to the Controller and Method with dependencies
        $this->callMethodInController();
    }


    private function getController(array $url = [])
    {
        if (isset($url[0])) {
            if (file_exists('../app/Controllers/' . $url[0] . '.php')) {
                $this->currentController = ucfirst($url[0]);
                unset($url[0]);
            }
        }
        require_once '../app/Controllers/' . $this->currentController . '.php';
        $this->currentController = new $this->currentController;
        return $url;
    }


    private function getMethod(array $url)
    {
        if (isset($url[1])) {

            if (method_exists($this->currentController, $url[1])) {
                $this->currentMethod = $url[1];
                unset($url[1]);
            }
        }
        return $url;
    }


    private function getDependencies(array $url)
    {
        if (isset($url[2])) {
            $this->dependencies = ($url[2]) ? array_values($url) : [];
        }
    }


    private function callMethodInController()
    {
        call_user_func_array([$this->currentController, $this->currentMethod], $this->dependencies);
    }
}