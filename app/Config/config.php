<?php
// Protecting sessions and cookies
ini_set('session.cookie_httponly', 1);

// start session
session_start();
ob_start();

// defining DB username
define('__DB_USER__', 'root');

// defining DB password
define('__DB_PASS__', '');

// defining DB host
define('__DB_HOST__', 'localhost');

// defining DB host
define('__DB_NAME__', 'mvc');


// defining the app name
define('__APP_NAME__', 'MatinMVC');

// defining the App root
define('__APP_ROOT__', 'http://localhost/matinmvc/');

// defining the App Dir
define('__APP_DIR__', dirname(dirname(__FILE__)));


// Mail configs

// defining The email
define('__USER_EMAIL__', 'matincoderweb@gmail.com');

// defining the password
define('__USER_PASSWORD', 'secret');

// defining the STMP Port
define('__EMAIL_PORT__', 587);

// defining the STMP Host
define('__EMAIL_HOST__', 'smtp1.example.com');
