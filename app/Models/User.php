<?php

class User
{
    private $db;

    public function __construct()
    {
        $this->db = new Database();
    }

    public function doesEmailExists($email)
    {
        $this->db->query('SELECT * FROM `mvc`.`users` WHERE email = :email');
        $this->db->bind('email', $email);
        $this->db->execute();
        return ($this->db->resultOne());
    }

    public function doesUsernameExists($name)
    {
        $this->db->query('SELECT * FROM  `users` WHERE name = :name');
        $this->db->bind('name', $name);
        $this->db->execute();
        return $this->db->resultOne();
    }

    public function storeUser($data)
    {
        $thisTime = date('Y-m-d H:i:s');
        $this->db->query("INSERT INTO mvc.users (`name`,`email`,`password`,`created_at`) VALUES (:name , :email, :password,:created_at)");
        $this->db->bind('name', $data['name']);
        $this->db->bind('email', $data['email']);
        $this->db->bind('password', $data['password']);
        $this->db->bind('created_at', $thisTime);
        return ($this->db->execute());
    }


    /// methods for login...

    public function passwordAndEmailCorrect($data)
    {
        $this->db->query('SELECT * FROM `mvc`.`users` WHERE email = :email');
        $this->db->bind('email', $data['email']);
        ($this->db->execute());
        return $this->db->resultOne();
    }
}