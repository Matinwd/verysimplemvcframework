<?php

class Pages extends Controller
{


    private $model;

    public function __construct()
    {
        $this->model = $this->model('Page');
    }

    public function about()
    {
        $this->view('about', ['awd' => 'waf']);
    }

    public function index()
    {
        $myData = $this->model->index();
        $data = [
            'users' => $myData
        ];
        $this->view('index', $data);
    }
}