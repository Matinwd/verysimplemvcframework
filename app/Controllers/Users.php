<?php

class  Users extends Controller
{
    private $model;
    public $data;

    public function __construct()
    {
        $this->model = $this->model('User');
    }

    public function register()
    {
        $this->data = [];
        if (!isset($_POST['submit'])) {
            $this->view('auth/register', $this->data);
            return;
        }
        $this->data = [
            'name' => $_POST['name'],
            'email' => $_POST['email'],
            'password' => $_POST['password'],
            'confirm_password' => $_POST['confirm_password'],
            'err_name' => null,
            'err_email' => null,
            'err_password' => null,
            'err_confirm_password' => null
        ];

        // hashing password

        // this is a simple text from matin


        $this->registerValidation();


        if (!$this->formRegisterValidated()) {
            $this->view('auth/register', $this->data);
            return;
        }
        $this->data['password'] = password_hash($this->data['password'], PASSWORD_DEFAULT);

        $storedUser = $this->model->storeUser($this->data);
        if ($storedUser) {


            myFlash('user', 'User created!', 'alert alert-success');

            // send email
            // sendMail('100ztaa@gmail.com', ['subject' => 'welcome', 'text' => 'Hello welcome to site'], []);

            $this->view('auth/authorization');
            return;

        }

    }

    public function login()
    {
        $this->data = [];
        if (!isset($_POST['submit'])) {
            $this->view('auth/login', $this->data);
            return;
        }
        $this->data = [
            'email' => $_POST['email'],
            'password' => $_POST['password'],
            'err_email' => null,
            'err_password' => null,
        ];


        $this->emailPasswordValidation();


        // is form valid?
        if (!$this->formLoginValidated()) {
            $this->view('auth/login', $this->data);
            return;
        };


        // an object of user if is valid
        $userResult = ($this->model->passwordAndEmailCorrect($this->data) ?? false);


        if (!$userResult) {
            $this->userNotExists();
            return;
        }

        // verifying password
        if (!$this->verifyPassword($this->data['password'], $userResult->password)) {
            $this->userNotExists();
            return;
        }

        $_SESSION['UserData'] = [
            'username' => $userResult->name,
            'email' => $userResult->email,
            'created_at' => $userResult->created_at,
            'activated_at' => $userResult->activated_at
        ];

        redirect('Pages/index');


    }


    /*
     * @return void
     *
     * */

    private function formRegisterValidated(): bool
    {
        return empty($this->data['err_name'])
            && empty($this->data['err_email'])
            && empty($this->data['err_password'])
            && empty($this->data['err_confirm_password']);
    }

    private function formLoginValidated(): bool
    {
        return empty($this->data['err_email'])
            && empty($this->data['err_password']);
    }

    private function registerValidation(): void
    {
        if (empty($this->data['name'])) {
            $this->data['err_name'] = 'Your name should be filled';
        }
        $this->emailPasswordValidation();

        if (empty($this->data['confirm_password'])) {
            $this->data['err_confirm_password'] = 'Your confirm password should be filled';
        } elseif (strlen($this->data['confirm_password']) < 6) {
            $this->data['err_confirm_password'] = 'Your confirm password should be 6 chars at least';
        } elseif ($this->data['password'] != $this->data['confirm_password']) {
            $this->data['err_confirm_password'] = 'Your confirm password should be match with password';
        }
        if ($this->model->doesEmailExists($this->data['email'])) {
            $this->data['err_email'] = 'This email is already exists';
        }
    }

    private function emailPasswordValidation(): void
    {
        if (empty($this->data['email'])) {
            $this->data['err_email'] = 'Your email should be filled';
        }
        if (empty($this->data['password'])) {
            $this->data['err_password'] = 'Your password should be filled';
        } elseif (strlen($this->data['password']) < 6) {
            $this->data['err_password'] = 'Your password should be 6 chars at least';
        }
    }

    private function verifyPassword(string $password, string $hash): bool
    {
        return password_verify($password, $hash);
    }

    private function userNotExists(): void
    {
        myFlash('user', 'User does not exists', 'alert alert-info');
        $this->view('auth/login', $this->data);
    }
}