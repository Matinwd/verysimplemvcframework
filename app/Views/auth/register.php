<? require_once __APP_DIR__ . "/Views/Sections/header.php"
?>
    <p class="text-center display-2">Register Page</p>

    <div class="container">
        <div class="row">
            <div class="col-6 offset-md-3">
                <div>
                    <?php myFlash('user') ?>
                </div>
                <form action="" method="post" class="form-group">
                    <div>
                        <label for="name">Name</label>
                        <input type="text" name="name" id="name"
                               value="<?php echo isset($data['name']) ? $data['name'] : '' ?>"
                               class="form-control">
                        <span><?php echo isset($data['err_name']) ? $data['err_name'] : '' ?></span>
                    </div>
                    <div>
                        <label for="email">Email</label>
                        <input type="email" id="email" name="email"
                               value="<?php echo isset($data['email']) ? $data['email'] : '' ?>" class="form-control">
                        <span class="error"><?php echo isset($data['err_email']) ? $data['err_email'] : '' ?></span>
                    </div>
                    <div>
                        <label for="password">password</label>
                        <input type="password" id="password" name="password"
                               value="<?php echo isset($data['password']) ? $data['password'] : '' ?>"
                               class="form-control">
                        <span class="error"><?php echo isset($data['err_password']) ? $data['err_password'] : '' ?></span>
                    </div>
                    <div>
                        <label for="confirm_password">confirm password</label>
                        <input type="password" id="confirm_password" name="confirm_password"
                               value="<?php echo isset($data['confirm_password']) ? $data['confirm_password'] : '' ?>"
                               class="form-control <?php echo !empty($data['err_confirm_password']) ? 'is_invalid' : '' ?> ">
                        <span class="is_invalid"><?php echo isset($data['err_confirm_password']) ? $data['err_confirm_password'] : '' ?></span>
                    </div>

                    <input type="submit" name="submit" class="btn btn-submit" value="Register">
                </form>
            </div>
        </div>
    </div>

<? require_once __APP_DIR__ . "/Views/Sections/Footer.php";