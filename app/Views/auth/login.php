<?php require_once __APP_DIR__ . "/Views/Sections/header.php" ?>


<p class="text-center display-2">Login Page</p>
<div>
    <div class="row">
        <div class="col-6 offset-md-3">
            <?php myFlash('user') ?>
            <form action="" method="post" class="form-group">
                <div>
                    <label for="email">Email</label>
                    <input type="email" name="email" id="email" class="form-control">
                    <span><?php echo isset($data['err_email']) ? $data['err_email'] : '' ?></span>
                </div>
                <div>
                    <label for="password">Password</label>
                    <input type="text" id="password" name="password" class="form-control">
                    <span><?php echo isset($data['err_password']) ? $data['err_password'] : '' ?></span>
                </div>

                <input type="submit" name="submit" class="btn btn-submit" value="Login">
            </form>
        </div>
    </div>
</div>


<?php require_once __APP_DIR__ . "/Views/Sections/Footer.php" ?>
